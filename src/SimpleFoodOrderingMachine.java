import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleFoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton ramenButton;
    private JButton gyozaButton;
    private JButton udonButton;
    private JButton sobaButton;
    private JButton karaageButton;
    private JButton tonkatuButton;
    private JButton checkOutButton;
    private JTextPane orderedItemsText;
    private JTextField textField1;
    int sum=0;

    void order(String food,int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+" ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            String currentText=orderedItemsText.getText();
            orderedItemsText.setText(currentText+food+" "+price+"yen"+"\n");
            JOptionPane.showMessageDialog(null,
                    "Order for "+food+" received.",
                    "Message",
                    JOptionPane.OK_OPTION);
            sum+=price;
            textField1.setText(sum+"yen");
        }
    }

    public SimpleFoodOrderingMachine() {
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                  order("Ramen",600);
            }
        });
        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("けやき味噌.jpg")
        ));
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza",300);
            }
        });
        gyozaButton.setIcon(new ImageIcon(
                this.getClass().getResource("sub114.jpg")
        ));
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",400);
            }
        });
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("src_16096588.jpg")
        ));
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba",450);
            }
        });
        sobaButton.setIcon(new ImageIcon(
                this.getClass().getResource("d7c5ff633ef5091bb3a4.jpg")
        ));
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage",200);
            }
        });
        karaageButton.setIcon(new ImageIcon(
                this.getClass().getResource("51c2ee6cf5612f570c2e2dc5.jpg")
        ));
        tonkatuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tonkatu",700);
            }
        });
        tonkatuButton.setIcon(new ImageIcon(
                this.getClass().getResource("73540_topImg.jpg")
        ));
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation2 = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation2==0){
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is "+sum+" yen.",
                            "Message",
                            JOptionPane.OK_OPTION);
                    sum=0;
                    textField1.setText(sum+"yen");
                    String currentText=orderedItemsText.getText();
                    orderedItemsText.setText("");
                }
            }
        });
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
